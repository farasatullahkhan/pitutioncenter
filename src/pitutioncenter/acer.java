/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pitutioncenter;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.TexturePaint;
import java.awt.geom.GeneralPath;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *
 * @author HP-PC
 */
class acer extends JPanel{
     
    BufferedImage image1=new BufferedImage(100,100,BufferedImage.TYPE_4BYTE_ABGR);
    int b=0;
    int g=255;
    int r=255;
    int a=0;
    int col=(a<<24)|(b<<16)|(g<<8)|r;    
 
     
    acer(){
 
         
        for(int i0=0;i0<60;i0++) {
            for(int i1=0;i1<60;i1++) {
                image1.setRGB(i0,i1,-col);
            }
        }
 
         
        this.setOpaque(true);
        this.setBackground(Color.red);
        this.setPreferredSize(new Dimension(900,700));
    }
 
     
    public void paintComponent(Graphics g) {
        Graphics2D g2d=(Graphics2D)g;
         
        int[] x2P= {640,690,740,890,740};
        int[] y2P= {550,400,550,600,650};
         
        GeneralPath path8=new GeneralPath(Path2D.WIND_EVEN_ODD,x2P.length);
        path8.moveTo(x2P[0], y2P[0]);
         
        for(int i=1;i<x2P.length;i++) {
            path8.lineTo(x2P[i], y2P[i]);
             
        }
        path8.closePath();
         
        TexturePaint texture0=new TexturePaint(image1,new Rectangle2D.Double(0,0,10,10));
         
        g2d.setPaint(texture0);
        g2d.fill(path8);
         
         
    }
     
     
}