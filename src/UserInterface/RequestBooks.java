/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.DatePickerSettings;
import com.github.lgooddatepicker.components.TimePickerSettings;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.DayOfWeek;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.border.Border;

/**
 *
 * @author HP-PC
 */
public class RequestBooks extends JFrame{
    
     JRadioButton maleBtn;
    JRadioButton femaleBtn;
    JRadioButton otherBtn;
    JRadioButton satisfiedBtn;
    JRadioButton verySatisfiedBtn;

    // Declaration of object of JButton class.
    JButton submitRatingBtn;
    JButton cancelRatingBtn;

    // Declaration of object of ButtonGroup class.
    ButtonGroup G1;
  
    JComboBox<String> subjectsList;
        String[] sL= {"Mental Math 1","Math","English Comprehension 3","Other"};
     JComboBox<String> lessonTimeList;
        String[] lesson_Time_List= {"10:00 AM to 12:00 AM","2:30 PM to 4:30 PM","7:00 PM to 9:00 PM"};
    
    // Declaration of object of  JLabel  class.
    JLabel ratingLabel;
  
    // Constructor of LessonRemarks class.
    public RequestBooks()
    {
        
//        ImagePanel imageBackgroundpanel = new ImagePanel(
//        new ImageIcon("C:\\Users\\HP-PC\\Desktop\\background9.jpg").getImage());
//        imageBackgroundpanel.setBounds(50, 14, 300, 300);
        
        JLabel lblNewUserRegister = new JLabel("Request Books");
        lblNewUserRegister.setFont(new Font("Times New Roman", Font.PLAIN, 38));
        lblNewUserRegister.setForeground(Color.decode("#29465B"));
        lblNewUserRegister.setBounds(160, 20, 325, 50);
        this.add(lblNewUserRegister);
        
        JPanel labelsBackgroundPanel = new JPanel(new GridLayout(6, 1, 10, 10));
        labelsBackgroundPanel.setBounds(120, 148, 120, 400);
//        labelsBackgroundPanel.setBackground(Color.decode("#045F5F"));
        
        

//        JLabel nameLabel = new JLabel("Name");
//        nameLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
//        nameLabel.setForeground(Color.decode("#29465B"));
////        dateLabel.setBounds(58, 20, 99, 43);
//        labelsBackgroundPanel.add(nameLabel);
        
        JLabel lblName = new JLabel("Subject");
        lblName.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblName.setForeground(Color.decode("#29465B"));
//        lblName.setBounds(58, 20, 99, 43);
        labelsBackgroundPanel.add(lblName);

        
        JLabel dateLabel = new JLabel("Date");
        dateLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
        dateLabel.setForeground(Color.decode("#29465B"));
//        dateLabel.setBounds(58, 20, 99, 43);
        labelsBackgroundPanel.add(dateLabel);
        

        JLabel timeLabel = new JLabel("Comments");
        timeLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
        timeLabel.setForeground(Color.decode("#29465B"));
//        timeLabel.setBounds(58, 20, 99, 43);
        labelsBackgroundPanel.add(timeLabel);
        
        
        JLabel lblPrice = new JLabel("Price");
        lblPrice.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblPrice.setForeground(Color.decode("#29465B"));
//        lblPrice.setBounds(58, 20, 99, 43);
        labelsBackgroundPanel.add(lblPrice);
        
        
//        JLabel ratingLabel = new JLabel("Gender");
//        ratingLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
//        ratingLabel.setForeground(Color.decode("#29465B"));
////        ratingLabel.setBounds(58, 20, 99, 43);
//        labelsBackgroundPanel.add(ratingLabel);
        
        subjectsList=new JComboBox<String>(sL);
        subjectsList.setBackground(Color.BLUE);
        subjectsList.setForeground(Color.WHITE);
        subjectsList.setFocusable(false);
        subjectsList.getComponent(0).setBackground(Color.green);
        subjectsList.getComponent(0).setForeground(Color.MAGENTA);
        subjectsList.setBounds(260, 163, 155, 30);
//        
//        JTextField nameField = new JTextField();
//        nameField.setText("Student Name");
//        nameField.setBounds(200, 163, 200, 33);
//        nameField.setFont(new Font("Tahoma", Font.PLAIN, 18));
//        this.add(nameField);

        JPanel DateBackgroundPanel = new JPanel();
        LayoutManager layout1 = new FlowLayout();  
        DateBackgroundPanel.setLayout(layout1);
        //        DateBackgroundPanel.setBackground(Color.WHITE);
        DateBackgroundPanel.setBounds(270, 228, 155, 40);

        DatePickerSettings dateSettings = new DatePickerSettings();
        dateSettings.setFirstDayOfWeek(DayOfWeek.MONDAY);

        DatePicker datePicker2 = new DatePicker(dateSettings);
        datePicker2.setBounds(270, 360, 110, 150);
        DatePickerSettings datePickerSettings = new DatePickerSettings();
        datePickerSettings.setFormatForDatesBeforeCommonEra("dd.MM.yyyy");
        datePickerSettings.setFormatForDatesCommonEra("dd.MM.yyyy");
        TimePickerSettings timePickerSettings = new TimePickerSettings();
        timePickerSettings.use24HourClockFormat();

        DatePicker datePicker = new DatePicker(datePickerSettings);
        DateBackgroundPanel.add(datePicker);
        datePicker.setDateToToday();
        
        this.add(DateBackgroundPanel);

//         lessonTimeList=new JComboBox<String>(lesson_Time_List);
//        lessonTimeList.setBackground(Color.BLUE);
//        lessonTimeList.setForeground(Color.WHITE);
//        lessonTimeList.setFocusable(false);
//        lessonTimeList.getComponent(0).setBackground(Color.green);
//        lessonTimeList.getComponent(0).setForeground(Color.MAGENTA);
//        lessonTimeList.setBounds(260, 300, 150, 30);
//        
            JTextArea commentArea = new  JTextArea();
            commentArea.setBounds(260, 300, 200, 60);
    this.add(commentArea);


        JLabel pricelabel = new JLabel("Quoted Price");

        pricelabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
        pricelabel.setForeground(Color.decode("#29465B"));
//        ratingLabel.setBounds(58, 20, 99, 43);
        pricelabel.setBounds(260, 366,200, 33);
        this.add(pricelabel);
        
//        JTextField tutorField = new JTextField();
//        tutorField.setText("Contact Number");
//        tutorField.setBounds(200, 430, 200, 35);
//        tutorField.setFont(new Font("Tahoma", Font.PLAIN, 18));
//        this.add(tutorField);

        

//        JPanel ratingsBackgroundPanel = new JPanel(new GridLayout(1, 2, 2, 2));
//        // Initialization of object of "JRadioButton" class.
//        maleBtn = new JRadioButton();
//        femaleBtn = new JRadioButton();
//        otherBtn = new JRadioButton();
//        satisfiedBtn = new JRadioButton();
//        verySatisfiedBtn = new JRadioButton();
//        
//        
//        maleBtn.setText("Male");
//        femaleBtn.setText("Female");
//        otherBtn.setText("Other");
//
//        ratingsBackgroundPanel.setBounds(200, 480, 420, 80);
//        ratingsBackgroundPanel.add(maleBtn);
//        ratingsBackgroundPanel.add(femaleBtn);
//        ratingsBackgroundPanel.add(otherBtn);
//
//        
//        this.add(ratingsBackgroundPanel);
        this.add(subjectsList);

        this.add(labelsBackgroundPanel);

        

       

        // Setting layout as null of JFrame.
        this.setLayout(null);




        


       
    // To display this picker, uncomment this line.
        

        



        // Initialization of object of "JButton" class.
        submitRatingBtn = new JButton("Request Books");
        cancelRatingBtn = new JButton("Back");
        submitRatingBtn.setBounds(365, 450, 120, 30);
        cancelRatingBtn.setBounds(245, 450, 100, 30);
        
        // Initialization of object of "ButtonGroup" class.
        G1 = new ButtonGroup();

        // Initialization of object of " JLabel" class.
//        ratingLabel = new JLabel("Rate");


        
        // Setting Bounds of RadioButton.

//        very_DissatisfiedBtn.setBounds(5, 15, 290, 30);
//        dissatisfiedBtn.setBounds(155, 40, 290, 30);
//        itsOkBtn.setBounds(190, 15, 30, 30);
//        satisfiedBtn.setBounds(220, 40, 30, 30);
//        verySatisfiedBtn.setBounds(250, 30, 30, 30);

        // Setting Bounds of submit Rating Button.

        
//        ratingsBackgroundPanel.setBackground(Color.WHITE);


//                screenBackgroundPanel.add(ratingsBackgroundPanel);

        // Setting Bounds of JLabel rating label.
//       ratingLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
//       ratingLabel.setBounds(58, 250, 150, 50);
//        this.add(ratingLabel);
        // "this" keyword in java refers to current object.

        
        Border border7=BorderFactory.createDashedBorder(new Color(0xA524FF), 2, 5, 4, true);

        


        

        
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // Adding  submit Rating Button on JFrame.
        this.add(cancelRatingBtn);
                
        this.add(submitRatingBtn);
//        this.add(screenBackgroundPanel);
        this.pack();
         
        this.setBackground(Color.decode("#eceef3"));
        // Adding Listener to submit Rating Button.
        submitRatingBtn.addActionListener(new ActionListener() {
  
            public void actionPerformed(ActionEvent e)
            {
                // Override Method
  
                String ratedAs = " ";
 
                // If condition to check if rating is selected.
                if (maleBtn.isSelected()) {
  
                    ratedAs = "Very dissatisfied";

                }
  
                else if (femaleBtn.isSelected()) {
  
                    ratedAs = "Dissatisfied";
                }
                  else if (otherBtn.isSelected()) {
  
                    ratedAs = "Ok";
                }
                    else if (satisfiedBtn.isSelected()) {
  
                    ratedAs = "Satisfied";
                }
                      else if (verySatisfiedBtn.isSelected()) {
  
                    ratedAs = "Very Satisfied";
                }
                else {
  
                    ratedAs = "NO Button selected";
                }
  
                // MessageDialog to show information selected radion buttons.
                JOptionPane.showMessageDialog(RequestBooks.this, ratedAs);
            }
        });
    }
}
