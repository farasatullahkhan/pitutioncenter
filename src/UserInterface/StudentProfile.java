/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.DatePickerSettings;
import com.github.lgooddatepicker.components.TimePickerSettings;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.time.DayOfWeek;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author HP-PC
 */
public class StudentProfile extends JFrame {
//    private static final long serialVersionUID = 1L;
//    private JPanel contentPane;
//    private JTextField firstname;
//    private JTextField lastname;
//    private JTextField email;
//    private JTextField username;
//    private JTextField mob;
//    private JPasswordField passwordField;
//    private JButton btnNewButton;
//   
//    /**
//     * Launch the application.
//     */
////    public static void main(String[] args) {
////        EventQueue.invokeLater(new Runnable() {
////            public void run() {
////                try {
////                    StudentProfile frame = new StudentProfile();
////                    frame.setVisible(true);
////                } catch (Exception e) {
////                    e.printStackTrace();
////                }
////            }
////        });
////    }
//
//    /**
//     * Create the frame.
//     */
//
//    public StudentProfile() {
//        
//        
//        setIconImage(Toolkit.getDefaultToolkit().getImage("background.png"));
//        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        setBounds(450, 190, 1014, 797);
//        setResizable(false);
//        contentPane = new JPanel();
//        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
//        setContentPane(contentPane);
//        contentPane.setLayout(null);
//
//        JLabel lblNewUserRegister = new JLabel("Student Registeration");
//        lblNewUserRegister.setFont(new Font("Times New Roman", Font.PLAIN, 42));
//        lblNewUserRegister.setBounds(362, 52, 325, 50);
//        contentPane.add(lblNewUserRegister);
//
//        JLabel lblName = new JLabel("Name");
//        lblName.setFont(new Font("Tahoma", Font.PLAIN, 20));
//        lblName.setBounds(58, 152, 99, 43);
//        contentPane.add(lblName);
//
//        JLabel lblNewLabel = new JLabel("DOB");
//        lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
//        lblNewLabel.setBounds(58, 243, 110, 29);
//        contentPane.add(lblNewLabel);
//
//        JLabel lblEmailAddress = new JLabel("Gender");
//        lblEmailAddress.setFont(new Font("Tahoma", Font.PLAIN, 20));
//        lblEmailAddress.setBounds(58, 324, 124, 36);
//        contentPane.add(lblEmailAddress);
//
//        firstname = new JTextField();
//        firstname.setFont(new Font("Tahoma", Font.PLAIN, 32));
//        firstname.setBounds(214, 151, 228, 50);
//        contentPane.add(firstname);
//        firstname.setColumns(10);
//
//        lastname = new JTextField();
//        lastname.setFont(new Font("Tahoma", Font.PLAIN, 32));
//        lastname.setBounds(214, 235, 228, 50);
//        contentPane.add(lastname);
//        lastname.setColumns(10);
//
//        email = new JTextField();
//
//        email.setFont(new Font("Tahoma", Font.PLAIN, 32));
//        email.setBounds(214, 320, 228, 50);
//        contentPane.add(email);
//        email.setColumns(10);
//
//        username = new JTextField();
//        username.setFont(new Font("Tahoma", Font.PLAIN, 32));
//        username.setBounds(707, 151, 228, 50);
//        contentPane.add(username);
//        username.setColumns(10);
//
//        JLabel lblUsername = new JLabel("Address");
//        lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 20));
//        lblUsername.setBounds(542, 159, 99, 29);
//        contentPane.add(lblUsername);
//
//        JLabel lblPassword = new JLabel("Subject");
//        lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 20));
//        lblPassword.setBounds(542, 245, 99, 24);
//        contentPane.add(lblPassword);
//
//        JLabel lblMobileNumber = new JLabel("Emergency Contact Number");
//        lblMobileNumber.setFont(new Font("Tahoma", Font.PLAIN, 20));
//        lblMobileNumber.setBounds(542, 329, 339, 26);
//        contentPane.add(lblMobileNumber);
//
//        mob = new JTextField();
//        mob.setFont(new Font("Tahoma", Font.PLAIN, 32));
//        mob.setBounds(707, 320, 228, 50);
//        contentPane.add(mob);
//        mob.setColumns(10);
//
//        passwordField = new JPasswordField();
//        passwordField.setFont(new Font("Tahoma", Font.PLAIN, 32));
//        passwordField.setBounds(707, 235, 228, 50);
//        contentPane.add(passwordField);
//
//        btnNewButton = new JButton("Register");
//        btnNewButton.addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//                String firstName = firstname.getText();
//                String lastName = lastname.getText();
//                String emailId = email.getText();
//                String userName = username.getText();
//                String mobileNumber = mob.getText();
//                int len = mobileNumber.length();
//                String password = passwordField.getText();
//
//                String msg = "" + firstName;
//                msg += " \n";
//                if (len != 10) {
//                    JOptionPane.showMessageDialog(btnNewButton, "Enter a valid mobile number");
//                }
//
//                try {
//                    Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/swing_demo", "root", "root");
//
//                    String query = "INSERT INTO account values('" + firstName + "','" + lastName + "','" + userName + "','" +
//                        password + "','" + emailId + "','" + mobileNumber + "')";
//                    java.sql.Statement sta = connection.createStatement();
//                    int x = sta.executeUpdate(query);
//                    if (x == 0) {
//                        JOptionPane.showMessageDialog(btnNewButton, "This is alredy exist");
//                    } else {
//                        JOptionPane.showMessageDialog(btnNewButton,
//                            "Welcome, " + msg + "Your account is sucessfully created");
//                    }
//                    connection.close();
//                } catch (Exception exception) {
//                    exception.printStackTrace();
//                }
//            }
//        });
//        btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 22));
//        btnNewButton.setBounds(399, 447, 259, 74);
//        contentPane.add(btnNewButton);
//    }
//}

    // Declaration of object of JRadioButton class.
    JRadioButton maleBtn;
    JRadioButton femaleBtn;
    JRadioButton otherBtn;
    JRadioButton satisfiedBtn;
    JRadioButton verySatisfiedBtn;

    // Declaration of object of JButton class.
    JButton submitRatingBtn;
    JButton cancelRatingBtn;

    // Declaration of object of ButtonGroup class.
    ButtonGroup G1;
  
    JComboBox<String> subjectsList;
        String[] sL= {"English","Math","Verbal Reasoning","Non-verbal Reasoning"};
     JComboBox<String> lessonTimeList;
        String[] lesson_Time_List= {"10:00 AM to 12:00 AM","2:30 PM to 4:30 PM","7:00 PM to 9:00 PM"};
    
    // Declaration of object of  JLabel  class.
    JLabel ratingLabel;
  
    // Constructor of LessonRemarks class.
    public StudentProfile()
    {
        
//        ImagePanel imageBackgroundpanel = new ImagePanel(
//        new ImageIcon("C:\\Users\\HP-PC\\Desktop\\background9.jpg").getImage());
//        imageBackgroundpanel.setBounds(50, 14, 300, 300);
        
        JLabel lblNewUserRegister = new JLabel("Student Profile");
        lblNewUserRegister.setFont(new Font("Times New Roman", Font.PLAIN, 38));
        lblNewUserRegister.setForeground(Color.decode("#29465B"));
        lblNewUserRegister.setBounds(160, 20, 325, 50);
        this.add(lblNewUserRegister);
        
        JPanel labelsBackgroundPanel = new JPanel(new GridLayout(6, 1, 10, 10));
        labelsBackgroundPanel.setBounds(60, 148, 100, 400);
//        labelsBackgroundPanel.setBackground(Color.decode("#045F5F"));
        
        

        JLabel nameLabel = new JLabel("Name");
        nameLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
        nameLabel.setForeground(Color.decode("#29465B"));
//        dateLabel.setBounds(58, 20, 99, 43);
        labelsBackgroundPanel.add(nameLabel);
        
        JLabel lblName = new JLabel("Subject");
        lblName.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblName.setForeground(Color.decode("#29465B"));
//        lblName.setBounds(58, 20, 99, 43);
        labelsBackgroundPanel.add(lblName);

        
        JLabel dateLabel = new JLabel("DOB");
        dateLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
        dateLabel.setForeground(Color.decode("#29465B"));
//        dateLabel.setBounds(58, 20, 99, 43);
        labelsBackgroundPanel.add(dateLabel);
        

        JLabel timeLabel = new JLabel("Address");
        timeLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
        timeLabel.setForeground(Color.decode("#29465B"));
//        timeLabel.setBounds(58, 20, 99, 43);
        labelsBackgroundPanel.add(timeLabel);
        
        
        JLabel lblPrice = new JLabel("Contact No");
        lblPrice.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblPrice.setForeground(Color.decode("#29465B"));
//        lblPrice.setBounds(58, 20, 99, 43);
        labelsBackgroundPanel.add(lblPrice);
        
        
        JLabel ratingLabel = new JLabel("Gender");
        ratingLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
        ratingLabel.setForeground(Color.decode("#29465B"));
//        ratingLabel.setBounds(58, 20, 99, 43);
        labelsBackgroundPanel.add(ratingLabel);
        
        subjectsList=new JComboBox<String>(sL);
        subjectsList.setBackground(Color.BLUE);
        subjectsList.setForeground(Color.WHITE);
        subjectsList.setFocusable(false);
        subjectsList.getComponent(0).setBackground(Color.green);
        subjectsList.getComponent(0).setForeground(Color.MAGENTA);
        subjectsList.setBounds(200, 228, 155, 30);
//        
        JTextField nameField = new JTextField();
        nameField.setText("Student Name");
        nameField.setBounds(200, 163, 200, 33);
        nameField.setFont(new Font("Tahoma", Font.PLAIN, 18));
        this.add(nameField);

        JPanel DateBackgroundPanel = new JPanel();
        LayoutManager layout1 = new FlowLayout();  
        DateBackgroundPanel.setLayout(layout1);
        //        DateBackgroundPanel.setBackground(Color.WHITE);
        DateBackgroundPanel.setBounds(210, 290, 155, 40);

        DatePickerSettings dateSettings = new DatePickerSettings();
        dateSettings.setFirstDayOfWeek(DayOfWeek.MONDAY);

        DatePicker datePicker2 = new DatePicker(dateSettings);
        datePicker2.setBounds(180, 360, 110, 150);
        DatePickerSettings datePickerSettings = new DatePickerSettings();
        datePickerSettings.setFormatForDatesBeforeCommonEra("dd.MM.yyyy");
        datePickerSettings.setFormatForDatesCommonEra("dd.MM.yyyy");
        TimePickerSettings timePickerSettings = new TimePickerSettings();
        timePickerSettings.use24HourClockFormat();

        DatePicker datePicker = new DatePicker(datePickerSettings);
        DateBackgroundPanel.add(datePicker);
        datePicker.setDateToToday();
        
        this.add(DateBackgroundPanel);

//         lessonTimeList=new JComboBox<String>(lesson_Time_List);
//        lessonTimeList.setBackground(Color.BLUE);
//        lessonTimeList.setForeground(Color.WHITE);
//        lessonTimeList.setFocusable(false);
//        lessonTimeList.getComponent(0).setBackground(Color.green);
//        lessonTimeList.getComponent(0).setForeground(Color.MAGENTA);
//        lessonTimeList.setBounds(200, 330, 150, 30);
//        
//        this.add(lessonTimeList);


        JTextField addressField = new JTextField();
        addressField.setText("Student Address");
        addressField.setBounds(200, 360,200, 33);
        addressField.setFont(new Font("Tahoma", Font.PLAIN, 18));
        this.add(addressField);
        
        JTextField tutorField = new JTextField();
        tutorField.setText("Contact Number");
        tutorField.setBounds(200, 430, 200, 35);
        tutorField.setFont(new Font("Tahoma", Font.PLAIN, 18));
        this.add(tutorField);

        

        JPanel ratingsBackgroundPanel = new JPanel(new GridLayout(1, 2, 2, 2));
        // Initialization of object of "JRadioButton" class.
        maleBtn = new JRadioButton();
        femaleBtn = new JRadioButton();
        otherBtn = new JRadioButton();
        satisfiedBtn = new JRadioButton();
        verySatisfiedBtn = new JRadioButton();
        
        
        maleBtn.setText("Male");
        femaleBtn.setText("Female");
        otherBtn.setText("Other");

        ratingsBackgroundPanel.setBounds(200, 480, 420, 80);
        ratingsBackgroundPanel.add(maleBtn);
        ratingsBackgroundPanel.add(femaleBtn);
        ratingsBackgroundPanel.add(otherBtn);

        
        this.add(ratingsBackgroundPanel);
        this.add(subjectsList);

        this.add(labelsBackgroundPanel);

        

       

        // Setting layout as null of JFrame.
        this.setLayout(null);




        


       
    // To display this picker, uncomment this line.
        

        



        // Initialization of object of "JButton" class.
        submitRatingBtn = new JButton("Submit");
        cancelRatingBtn = new JButton("Cancel");
        submitRatingBtn.setBounds(305, 600, 80, 30);
        cancelRatingBtn.setBounds(205, 600, 80, 30);
        
        // Initialization of object of "ButtonGroup" class.
        G1 = new ButtonGroup();

        // Initialization of object of " JLabel" class.
//        ratingLabel = new JLabel("Rate");


        
        // Setting Bounds of RadioButton.

//        very_DissatisfiedBtn.setBounds(5, 15, 290, 30);
//        dissatisfiedBtn.setBounds(155, 40, 290, 30);
//        itsOkBtn.setBounds(190, 15, 30, 30);
//        satisfiedBtn.setBounds(220, 40, 30, 30);
//        verySatisfiedBtn.setBounds(250, 30, 30, 30);

        // Setting Bounds of submit Rating Button.

        
//        ratingsBackgroundPanel.setBackground(Color.WHITE);


//                screenBackgroundPanel.add(ratingsBackgroundPanel);

        // Setting Bounds of JLabel rating label.
//       ratingLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
//       ratingLabel.setBounds(58, 250, 150, 50);
//        this.add(ratingLabel);
        // "this" keyword in java refers to current object.

        
        Border border7=BorderFactory.createDashedBorder(new Color(0xA524FF), 2, 5, 4, true);

        


        

        
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // Adding  submit Rating Button on JFrame.
        this.add(cancelRatingBtn);
                
        this.add(submitRatingBtn);
//        this.add(screenBackgroundPanel);
        this.pack();
         
        this.setBackground(Color.decode("#eceef3"));
        // Adding Listener to submit Rating Button.
        submitRatingBtn.addActionListener(new ActionListener() {
  
            public void actionPerformed(ActionEvent e)
            {
                // Override Method
  
                String ratedAs = " ";
 
                // If condition to check if rating is selected.
                if (maleBtn.isSelected()) {
  
                    ratedAs = "Very dissatisfied";

                }
  
                else if (femaleBtn.isSelected()) {
  
                    ratedAs = "Dissatisfied";
                }
                  else if (otherBtn.isSelected()) {
  
                    ratedAs = "Ok";
                }
                    else if (satisfiedBtn.isSelected()) {
  
                    ratedAs = "Satisfied";
                }
                      else if (verySatisfiedBtn.isSelected()) {
  
                    ratedAs = "Very Satisfied";
                }
                else {
  
                    ratedAs = "NO Button selected";
                }
  
                // MessageDialog to show information selected radion buttons.
                JOptionPane.showMessageDialog(StudentProfile.this, ratedAs);
            }
        });
    }
}