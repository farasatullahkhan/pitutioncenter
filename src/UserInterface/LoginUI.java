/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import javax.swing.*;
import java.awt.*;
import static java.awt.SystemColor.text;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.GeneralPath;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import pitutioncenter.ImagePanel;

public class LoginUI  extends JFrame implements ActionListener {
 
     Container container=getContentPane();
    JLabel userLabel=new JLabel("USERNAME");
    JLabel passwordLabel=new JLabel("PASSWORD");
    JTextField userTextField=new JTextField();
    JPasswordField passwordField=new JPasswordField();
    JButton loginButton=new JButton("LOGIN");
    JButton resetButton=new JButton("RESET");
    JCheckBox showPassword=new JCheckBox("Show Password");
//    private ImageIcon image1;
    JLabel backgroundLabel;
JMenuBar mb;    
JMenu file,edit,help;    
JMenuItem cut,copy,paste,selectAll;    
JTextArea ta;    
  JComboBox<String> lessonTimeList;
        String[] lesson_Time_List= {"10:00 AM to 12:00 AM","2:30 PM to 4:30 PM","7:00 PM to 9:00 PM"};  

        
    //Creating constructor of LoginFrame() class
   public LoginUI()
    {
 //Calling setLayoutManger() method inside the constructor.
//        image1 = new ImageIcon(getClass().getResource("C:/Users/HP-PC/Desktop/background.png"));
//        backgroundLabel = new JLabel(image1);
//        add(backgroundLabel);
//        
//         setContentPane(new JLabel(new ImageIcon("background.png")));
        
        
        lessonTimeList=new JComboBox<String>(lesson_Time_List);
        lessonTimeList.setBackground(Color.BLUE);
        lessonTimeList.setForeground(Color.WHITE);
        lessonTimeList.setFocusable(false);
        lessonTimeList.getComponent(0).setBackground(Color.green);
        lessonTimeList.getComponent(0).setForeground(Color.MAGENTA);
        lessonTimeList.setBounds(200, 130, 150, 30);
        
        this.add(lessonTimeList);
        
        setLayoutManager();
        setLocationAndSize();
        addComponentsToContainer();
        addActionEvent();//calling addActionEvent() method
     }
 
   public void setLayoutManager()
   {
       container.setLayout(null);
   }
   public void setLocationAndSize()
   {
       //Setting location and Size of each components using setBounds() method.

       userLabel.setBounds(50,150,100,30);
       passwordLabel.setBounds(50,220,100,30);
       userTextField.setBounds(150,150,150,30);
       passwordField.setBounds(150,220,150,30);
       showPassword.setBounds(150,250,150,30);
       loginButton.setBounds(50,300,100,30);
       resetButton.setBounds(200,300,100,30);
 
 
   }
   
   public void addComponentsToContainer()
   {
      //Adding each components to the Container
       container.add(userLabel);
       container.add(passwordLabel);
       container.add(userTextField);
       container.add(passwordField);
       container.add(showPassword);
       container.add(loginButton);
       container.add(resetButton);

   }
  public void addActionEvent()
   {
      //adding Action listener to components
       loginButton.addActionListener(this);
       resetButton.addActionListener(this);
       showPassword.addActionListener(this);
   }
    //Overriding actionPerformed() method
    @Override
    public void actionPerformed(ActionEvent e) {
 //Coding Part of LOGIN button
        if (e.getSource() == loginButton) {
            String userText;
            String pwdText;
            userText = userTextField.getText();
            pwdText = passwordField.getText();
            if (userText.equalsIgnoreCase("farasat") && pwdText.equalsIgnoreCase("123")) {
                JOptionPane.showMessageDialog(this, "Login Successful");
                StudentProfile sp = new StudentProfile();
                this.dispose();
                JMenuBar mb=new JMenuBar();  
       cut=new JMenuItem("cut");    
copy=new JMenuItem("copy");    
paste=new JMenuItem("paste");    
selectAll=new JMenuItem("selectAll");    
cut.addActionListener(this);    
copy.addActionListener(this);    
paste.addActionListener(this);    
selectAll.addActionListener(this);    
mb=new JMenuBar();    
file=new JMenu("File");    
edit=new JMenu("Edit");    
help=new JMenu("Help");     
edit.add(cut);
edit.add(copy);
edit.add(paste);
edit.add(selectAll);  
mb.add(file);
mb.add(edit);
mb.add(help);    
ta=new JTextArea(); 
sp.add(mb);
sp.add(ta);
  sp.setJMenuBar(mb);  
  
  if(e.getSource()==cut)    
ta.cut();    
if(e.getSource()==paste)    
ta.paste();    
if(e.getSource()==copy)    
ta.copy();    
if(e.getSource()==selectAll)    
ta.selectAll();    

                sp.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(this, "Invalid Username or Password");
            }
 
        }
        //Coding Part of RESET button
        if (e.getSource() == resetButton) {
            userTextField.setText("");
            passwordField.setText("");
        }
       //Coding Part of showPassword JCheckBox
        if (e.getSource() == showPassword) {
            if (showPassword.isSelected()) {
                passwordField.setEchoChar((char) 0);
            } else {
                passwordField.setEchoChar('*');
            }
 
 
        }
    }
  
}



//extends JFrame  {
//        JFrame f;  
//        private JPanel panel = new JPanel(new BorderLayout(5,5));
//        private JPanel btnPanel = new JPanel(new GridLayout(5,3,2,2));
//            
//     public LoginUI(){
//          displayingChooseOptionDialog();
//        }
//     
//     
//     
//     
//     
//    void displayingChooseOptionDialog(){
//        setTitle("Pi Tution Center Login Screen");
//        panel.setBackground(Color.BLACK);
//        btnPanel.setBackground(Color.BLACK);
//       
//       JTextField sampleField = new JTextField(5);
//    GridBagConstraints c = new GridBagConstraints();
//    c.anchor = GridBagConstraints.WEST;
//    c.gridx = 0;
//    c.gridy = 0;
//    c.insets = new Insets(5, 5, 5, 5);
//
//    JLabel sampleLabel = new JLabel("Username/Id");
//      Font font = new Font("Serif", Font.BOLD, 18);
////
//      sampleLabel.setFont(font);
//    sampleLabel.setForeground(Color.WHITE);
//
//    sampleLabel.setForeground(Color.WHITE);
//
//    
//    btnPanel.add(sampleLabel,c);
//
//    c.gridy++;
//    btnPanel.add(sampleField,c);
//    c.gridy++;
//  
//        JButton button = new JButton("LoginIn");
//       button.addActionListener(new ButtonListeners());                    // (5)
////       frame.getContentPane().add(button);
//       
//       
//       
//        btnPanel.add(button);
//        panel.add(btnPanel, BorderLayout.CENTER);
//        //       JButton button = new JButton("LoginIn");
////       button.addActionListener(new ButtonListeners())
//        add(panel);
//
//
//        setSize(340, 400);
//        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        setVisible(true);
//
////       JFrame frame = new JFrame("Pi Tution Center Login Screen");
////       frame.setSize(600,600);                 
////       JButton button = new JButton("LoginIn");
////       button.addActionListener(new ButtonListeners());                    // (5)
////       frame.getContentPane().add(button); // Adds Button to content pane of frame
////       String cakeString = "cake";
////        final JLabel piclabel
////                = new JLabel(new ImageIcon( cakeString + ".jpg"));
////        
////         JPanel panel = new JPanel(new GridBagLayout());
////    JTextField sampleField = new JTextField(5);
////    GridBagConstraints c = new GridBagConstraints();
////    c.anchor = GridBagConstraints.WEST;
////    c.gridx = 0;
////    c.gridy = 0;
////    c.insets = new Insets(5, 5, 5, 5);
////
////    JLabel sampleLabel = new JLabel("sample text");
////    panel.add(sampleLabel,c);
////
////    c.gridy++;
////    panel.add(sampleField,c);
////
////    c.gridy++;
////    panel.add(new JLabel("sample text 2"),c);
////
////    c.gridy++;
////    panel.add(new JTextField(5),c);
////
////    c.gridy++;
////    panel.add(new JTextField(5),c);
////        
////        frame.add(panel);
//////       frame.getContentPane().add(piclabel);
//////       setLocationRelativeTo(null);
////       frame.setVisible(true);
//        }
//         public void windowClosing() {  
//            
//        int a=JOptionPane.showConfirmDialog(f,"Are you sure?");  
//        if(a==JOptionPane.YES_OPTION){  
//        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
//        
//        }  
//        }  
//
// 
//}
