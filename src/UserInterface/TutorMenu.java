/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author HP-PC
 */
public class TutorMenu extends JFrame {
 public TutorMenu(){
    
     JLabel loginpitLabel = new JLabel();
            loginpitLabel.setText("Tutor Menu");
            loginpitLabel.setForeground(Color.BLACK);
            loginpitLabel.setBounds(200, 0, 370, 200);
            loginpitLabel.setFont(new Font("Times New Roman", Font.PLAIN, 38));

            
            JPanel btnView = new JPanel(new GridLayout(3, 1, 10, 10));

            JButton bookLessonsButton=new JButton("Student Progress Review");
            bookLessonsButton.addActionListener(new ActionListener() {

    @Override
    public void actionPerformed(ActionEvent e) {
               JOptionPane.showConfirmDialog(null, "Dear Tutor you are heading towards Student Progress Review Screen!!", "Transition Successfull...",
			JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
        TutorRemarks tutorRemarksFrame;
        tutorRemarksFrame = new TutorRemarks();
        tutorRemarksFrame.setBounds(10, 10, 600, 700);
        tutorRemarksFrame.setVisible(true);
               
    }
});
            btnView.add(bookLessonsButton);
            
            JButton CancelLessonsButton=new JButton("Ammend BookList");
            
            CancelLessonsButton.addActionListener(new ActionListener() {

    @Override
    public void actionPerformed(ActionEvent e) {
               JOptionPane.showConfirmDialog(null, "Dear Tutor you are heading towards Ammend  BooklList  Screen!!", "Transition Successfull...",
			JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
        AmmendBookList ammendBookListFrame;
        ammendBookListFrame = new AmmendBookList();
        ammendBookListFrame.setBounds(10, 10, 600, 700);
        ammendBookListFrame.setVisible(true);
               
    }
            });
            btnView.add(CancelLessonsButton);
            
            JButton generateStudentReportButton=new JButton("Generate Student Report");
            generateStudentReportButton.addActionListener(new ActionListener() {

    @Override
    public void actionPerformed(ActionEvent e) {
               JOptionPane.showConfirmDialog(null, "Dear Tutor you are heading towards Generate  Student Report  Screen!!", "Transition Successfull...",
			JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
        StudentProgressReport studentProgressReportFrame;
        studentProgressReportFrame = new StudentProgressReport();
        studentProgressReportFrame.setBounds(10, 10, 600, 700);
        studentProgressReportFrame.setVisible(true);
               
    }
            });
            btnView.add(generateStudentReportButton);
            
           
            btnView.setBounds(120, 200, 320, 200);

            
            this.add(loginpitLabel);
            
            this.add(btnView);
            this.setLayout(null);
            
    }
}
