/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import com.sun.xml.internal.bind.CycleRecoverable.Context;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author HP-PC
 */
public class loginPTC  extends JFrame implements ActionListener{
    
    
    JComboBox<String> userTypeList;
        String[] user_Type_List= {"Student","Teacher","ShopKeeper"};  
    JTextField uniqueIDField = new JTextField();
String newID ;
        
    public loginPTC(){
    
        
            JLabel loginpitLabel = new JLabel();
            loginpitLabel.setText("PiTution Center Login");
            loginpitLabel.setForeground(Color.BLACK);
            loginpitLabel.setBounds(140, 0, 370, 200);
            loginpitLabel.setFont(new Font("Times New Roman", Font.PLAIN, 38));

            this.add(loginpitLabel);
            
            JPanel labelView = new JPanel(new GridLayout(4, 1, 10, 10));
            JPanel fieldsView = new JPanel(new GridLayout(4, 1, 10, 10));
            JPanel contentView = new JPanel(new GridLayout(1, 1, 10, 10));

            JLabel userTypeLabel = new JLabel();
            userTypeLabel.setText("Select User Type");
            userTypeLabel.setFont(new Font("Times New Roman", Font.PLAIN, 18));

            labelView.add(userTypeLabel);

            



            userTypeList=new JComboBox<String>(user_Type_List);
            userTypeList.setBackground(Color.BLUE);
            userTypeList.setForeground(Color.WHITE);
            userTypeList.setFocusable(false);
            userTypeList.getComponent(0).setBackground(Color.green);
            userTypeList.getComponent(0).setForeground(Color.MAGENTA);
            userTypeList.setBounds(200, 130, 80, 30);

            fieldsView.add(userTypeList);

            
            JLabel userUniqueIdLabel = new JLabel();
            userUniqueIdLabel.setText("Enter Unique Id");
           userUniqueIdLabel.setFont(new Font("Times New Roman", Font.PLAIN, 18));

            labelView.add(userUniqueIdLabel);
            uniqueIDField.setText(newID);
            fieldsView.add(uniqueIDField);


             JButton loginButton=new JButton("LOGIN");
             loginButton.setBounds(350, 400, 100, 50);
            this.add(loginButton);


            labelView.setBounds(60, 200, 200, 200);
            fieldsView.setBounds(280, 200, 200, 200);
//fieldsView.setBackground(Color.GRAY);
//            labelView.setBackground(Color.red);
            contentView.setBounds(30, 260, 500, 200);

            contentView.add(labelView);
            contentView.add(fieldsView);
            
           loginButton.addActionListener(this);
           setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            this.add(contentView);
            this.setLayout(null);


    }

    @Override
    public void actionPerformed(ActionEvent e) {
 Map<Integer, String> myUsers = new HashMap<Integer, String>(100, 100);
myUsers.put(userTypeList.getSelectedIndex(), uniqueIDField.getText());

    
    if (uniqueIDField.getText().toString().isEmpty())
{
   JOptionPane.showConfirmDialog(null, "Dear User Enter some Unique ID in the TextField First !!", "Login Error...",
			JOptionPane.PLAIN_MESSAGE, JOptionPane.ERROR_MESSAGE);
           setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

}
    else {
    switch(userTypeList.getSelectedIndex())
{

    case 0:
        
        
   newID =  uniqueIDField.getText();
         JOptionPane.showConfirmDialog(this, 
                "User Logged In using Unique ID of \" "+uniqueIDField.getText() + " \" and with a role of " + "\" " +userTypeList.getSelectedItem().toString() + "\"" , "Customized Dialog", 
                JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
     
        StudentMenu studentFrame;
        studentFrame = new StudentMenu();
        studentFrame.setBounds(10, 10, 600, 700);
        studentFrame.setVisible(true);
        this.dispose();

        break;
    case 1:
        
  newID =  uniqueIDField.getText();
         JOptionPane.showConfirmDialog(this, 
                "User Logged In using Unique ID of \" "+uniqueIDField.getText() + " \" and with a role of " + "\" " +userTypeList.getSelectedItem().toString() + "\"" , "Customized Dialog", 
                JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
        TutorMenu tutorFrame;
        tutorFrame = new TutorMenu();
        tutorFrame.setBounds(10, 10, 600, 700);
        tutorFrame.setVisible(true);
        this.dispose();
        break;
    case 2: 
        
  newID =  uniqueIDField.getText();
         JOptionPane.showConfirmDialog(this, 
                "User Logged In using Unique ID of \" "+uniqueIDField.getText() + " \" and with a role of " + " \" " +userTypeList.getSelectedItem().toString() + "\"" , "Customized Dialog", 
                JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
      
        shopKeeperMenu shopKeeperFrame;
        shopKeeperFrame = new shopKeeperMenu();
        shopKeeperFrame.setBounds(10, 10, 600, 700);
        shopKeeperFrame.setVisible(true);
                this.dispose();

        break;
    default:
        
        break;
}

    
    }

    }
    
}
