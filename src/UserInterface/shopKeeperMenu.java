/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author HP-PC
 */
public class shopKeeperMenu extends JFrame {
    
    public shopKeeperMenu(){
    
        
    
     JLabel loginpitLabel = new JLabel();
            loginpitLabel.setText("ShopKeeper Menu");
            loginpitLabel.setForeground(Color.BLACK);
            loginpitLabel.setBounds(150, 0, 370, 200);
            loginpitLabel.setFont(new Font("Times New Roman", Font.PLAIN, 38));

            
            JPanel btnView = new JPanel(new GridLayout(3, 1, 10, 10));

            JButton retrieveCurrentBooksButton=new JButton("Retrieve Current Book List");
            retrieveCurrentBooksButton.addActionListener(new ActionListener() {

    @Override
    public void actionPerformed(ActionEvent e) {
               JOptionPane.showConfirmDialog(null, "Dear ShopKeeper you are heading towards Retrieve Current Book List  Screen!!", "Transition Successfull...",
			JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
       
        RetrieveBookList currentBookListFrame = new RetrieveBookList();
        currentBookListFrame.setBounds(10, 10, 600, 700);
        currentBookListFrame.setVisible(true);
    }
            });
            btnView.add(retrieveCurrentBooksButton);
            

            
            JButton generateStudentReportButton=new JButton("Available Book list");
            generateStudentReportButton.addActionListener(new ActionListener() {

    @Override
    public void actionPerformed(ActionEvent e) {
               JOptionPane.showConfirmDialog(null, "Dear ShopKeeper you are heading towards Available Book list Screen!!", "Transition Successfull...",
			JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
            AvailableBookList availableBookListFrame = new AvailableBookList();
            availableBookListFrame.setBounds(10, 10, 600, 700);
            availableBookListFrame.setVisible(true);
    }
            });
            btnView.add(generateStudentReportButton);
            
           
            btnView.setBounds(120, 200, 320, 200);

            
            this.add(loginpitLabel);
            
            this.add(btnView);
            this.setLayout(null);
            
    }
    
    
}
