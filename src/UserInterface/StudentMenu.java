/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author HP-PC
 */
public class StudentMenu extends JFrame {
    
    public StudentMenu(){
    
     JLabel loginpitLabel = new JLabel();
            loginpitLabel.setText("Student Menu");
            loginpitLabel.setForeground(Color.BLACK);
            loginpitLabel.setBounds(200, 0, 370, 200);
            loginpitLabel.setFont(new Font("Times New Roman", Font.PLAIN, 38));

            
            JPanel btnView = new JPanel(new GridLayout(4, 3, 10, 10));

            JButton bookLessonsButton=new JButton("Book Lessons");
            bookLessonsButton.addActionListener(new ActionListener() {

    @Override
    public void actionPerformed(ActionEvent e) {
     JOptionPane.showConfirmDialog(null, "Dear Student you are heading towards Book Lesson Screen!!", "Transition Successfull...",
			JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
             

       BookLesson bookLessonFrame;
        bookLessonFrame = new BookLesson();
        bookLessonFrame.setBounds(10, 10, 600, 700);
        bookLessonFrame.setVisible(true);
    }
}
            );

            btnView.add(bookLessonsButton);
            
            JButton CancelLessonsButton=new JButton("Cancel Lessons");
            CancelLessonsButton.addActionListener(new ActionListener() {

    @Override
    public void actionPerformed(ActionEvent e) {
   JOptionPane.showConfirmDialog(null, "Dear Student you are heading towards Cancel Lesson Screen!!", "Transition Successfull...",
			JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
       CancelLessons cancelLessonFrame;
        cancelLessonFrame = new CancelLessons();
        cancelLessonFrame.setBounds(10, 10, 600, 700);
        cancelLessonFrame.setVisible(true);
    }
});

            btnView.add(CancelLessonsButton);
            
            JButton attendLessonsButton=new JButton("Check Into Lesson");
            attendLessonsButton.addActionListener(new ActionListener() {

    @Override
    public void actionPerformed(ActionEvent e) {
               JOptionPane.showConfirmDialog(null, "Dear Student you are heading towards Attend Lesson Screen!!", "Transition Successfull...",
			JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
StudentAttendance attendLessonFrame;
        attendLessonFrame = new StudentAttendance();
        attendLessonFrame.setBounds(10, 10, 600, 700);
        attendLessonFrame.setVisible(true);
               
    }
});

            btnView.add(attendLessonsButton);
            
            
            JButton markLessonReviewButton=new JButton("Provide Lesson Review");
            markLessonReviewButton.addActionListener(new ActionListener() {

    @Override
    public void actionPerformed(ActionEvent e) {
                JOptionPane.showConfirmDialog(null, "Dear Student you are heading towards Mark Lesson Review Screen!!", "Transition Successfull...",
			JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
        LessonRemarks lessonRemarksFrame;
        lessonRemarksFrame = new LessonRemarks();
        lessonRemarksFrame.setBounds(10, 10, 600, 700);
        lessonRemarksFrame.setVisible(true);
        
    }
});

            btnView.add(markLessonReviewButton);
            
                        
            JButton requestBooksButton=new JButton("Request Books");
            requestBooksButton.addActionListener(new ActionListener() {

    @Override
    public void actionPerformed(ActionEvent e) {
              JOptionPane.showConfirmDialog(null, "Dear Student you are heading towards Request Books Screen!!", "Transition Successfull...",
			JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

        RequestBooks requestBookFrame;
        requestBookFrame = new RequestBooks();
        requestBookFrame.setBounds(10, 10, 600, 700);
        requestBookFrame.setVisible(true);
    }
});

;
            btnView.add(requestBooksButton);
            
            JButton studentProfileButton=new JButton("Enter Student Info");
            studentProfileButton.addActionListener(new ActionListener() {

    @Override
    public void actionPerformed(ActionEvent e) {
                JOptionPane.showConfirmDialog(null, "Dear Student you are heading towards Student Profile Screen!!", "Transition Successfull...",
			JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);

        StudentProfile studentProfileFrame;
        studentProfileFrame = new StudentProfile();
        studentProfileFrame.setBounds(10, 10, 600, 700);
        studentProfileFrame.setVisible(true);
    }
});
            btnView.add(studentProfileButton);
            
             JButton viewStudentInfoButton=new JButton("View Student Info");
viewStudentInfoButton.addActionListener(new ActionListener() {

    @Override
    public void actionPerformed(ActionEvent e) {
                JOptionPane.showConfirmDialog(null, "Dear Student you are heading towards View Student Info Screen!!", "Transition Successfull...",
			JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE);
//
        ViewStudentInfo viewStudentInfoFrame = new ViewStudentInfo();
        viewStudentInfoFrame.setBounds(10, 10, 600, 700);
        viewStudentInfoFrame.setVisible(true);

   
    }
    
});
            btnView.add(viewStudentInfoButton);

            btnView.setBounds(150, 200, 330, 260);

            
            this.add(loginpitLabel);
           setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.add(btnView);
            this.setLayout(null);
            
    }
     
      


//    @Override
//    public void actionPerformed(ActionEvent e) {
//
//
//    }

  

}
